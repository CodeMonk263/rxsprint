package rx;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] argv) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

        System.out.println(Thread.currentThread().getName());
        Observable<Integer> observable = Observable.fromIterable(list);
        Disposable disposable = observable
                .filter(i -> i%2 == 0)
                .subscribe(System.out::println);

        // Android thing, memory leaks!
        if (!disposable.isDisposed()) {
            disposable.dispose();
            System.out.println("Disposed");
        }

        System.out.println("Hello world");
    }
}
